class RobotMoving:
    """
    Robot Moving Program
    """
    def __init__(self):
        """
        Constructor for initializing the initial values
        """
        self.current_row = 0
        self.current_column = 0
        self.current_direction = "S"

    def movement(self):
        """
        Moves the robot 1 step based on self.current_direction direction
        """
        if self.current_direction == "S" and self.current_row < 3:
            self.current_row += 1
        elif self.current_direction == "E" and self.current_column < 4:
            self.current_column += 1
        elif self.current_direction == "N" and self.current_row > 0:
            self.current_row -= 1
        elif self.current_direction == "W" and self.current_column > 0:
            self.current_column -= 1

    def robot_direction(self, direction: str):
        """
        Rotates the robot for particular direction
        :param direction: str
        """
        if direction == "E":
            if self.current_direction == direction or self.current_direction == "W":
                pass
            elif self.current_column in range(0, 4):
                self.current_column += 1
                self.current_direction = "E"
        elif direction == "W":
            if self.current_direction == direction or self.current_direction == "E":
                pass
            elif self.current_column > 0:
                self.current_column -= 1
                self.current_direction = "W"
        elif direction == "N":
            if self.current_direction == direction or self.current_direction == "S" or self.current_row == 0:
                pass
            else:
                self.current_row -= 1
                self.current_direction = "N"
        elif direction == "S":
            if self.current_direction == direction or self.current_direction == "N":
                pass
            elif self.current_row in range(0, 3):
                self.current_row += 1
                self.current_direction = "S"

    def robot_position(self, user_input: str):
        """
        Finds the robot current position and direction
        :param user_input: str
        :return:
            self.current_row: int
            self.current_column: int
            self.current_direction: str
        """
        for move_direction in user_input:
            if move_direction == "M":
                if self.current_column <= 4 and self.current_row <= 3:
                    self.movement()
            elif move_direction in ["E", "W", "N", "S"]:
                self.robot_direction(move_direction)
        return self.current_row, self.current_column, self.current_direction


if __name__ == "__main__":
    """
    Starts the robot movement until user input is 0
    """
    print("Please enter the instruction that contains only ['E', 'W', 'N', 'S', 'M'] values: Type 0 for exit")
    while True:
        robot_moving = RobotMoving()
        instructions = str(input("COMMAND: "))
        if instructions == "0":
            break
        response = robot_moving.robot_position(instructions)
        print(f"Robot Location: {response}")

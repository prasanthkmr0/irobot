# iRobot

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/prasanthkmr0/irobot.git
```

Go to the project directory

```bash
  cd irobot
```


Run the program

```bash
  python robot.py
```


## Tech Stack

**Programming Language:** Python

## FAQ

#### what happens if the user enters other than the specified values?

Ans: The current verion will take the input and do nothing

#### what happens if the user enters command in smaller case?

Ans: The current verion will take the input and do nothing for those values


## Author

- [@Prasanth](https://gitlab.com/prasanthkmr0)

